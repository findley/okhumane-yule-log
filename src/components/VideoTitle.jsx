import React from 'react';
import PropTypes from 'prop-types';

const containerStyle = {
  position: 'absolute',
  display: 'flex',
  alignItems: 'flex-start',
  justifyContent: 'center',
  flexWrap: 'wrap',
  top: '0',
  left: '0',
  right: '0',
  height: '100vh',
  paddingTop: '40px',
  textAlign: 'center',
  backgroundColor: 'rgba(0, 0, 0, 0.3)',
};

function VideoTitle({children, sub}) {

  return (
    <div style={containerStyle}>
      <div className="text-title">{children}</div>
    </div>
  );
}

VideoTitle.propTypes = {
  children: PropTypes.node.isRequired,
  sub: PropTypes.string,
};

VideoTitle.defaultProps = {
    sub: '',
};

export default VideoTitle;
