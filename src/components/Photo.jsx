import React from 'react';
import PropTypes from 'prop-types';

const style = {
  backgroundColor: '#210606',
  width: '85%',
  margin: '25px',
};

function Photo({ url }) {
  return (
    <img src={url} style={style} alt="" />
  );
}

Photo.propTypes = {
  url: PropTypes.string.isRequired,
};

export default Photo;
