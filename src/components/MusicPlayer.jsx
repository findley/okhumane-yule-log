import React from 'react';
import ReactJkMusicPlayer from 'react-jinke-music-player';
import "react-jinke-music-player/assets/index.css";

const options = {
 audioLists: [
    {
      name: "Oh Little Town of Bethlehem",
      singer: "Trinity Choir",
      cover: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSdv2FMoYgmrAbFmz34h8Fxpy-AZqaf8ZPWr2ClBRgDBWiJUTgSuA",
      musicSrc: "/yule/Trinity_Choir_-_13_-_Oh_Little_Town_of_Bethlehem_1916.mp3",
    },
  ],
  remove: false,
  mode: 'mini',
  autoPlay: true,
  drag: true,
  toggleMode: true,
  showThemeSwitch: false,
  showLyric: false,
  showDownload: false,
  showReload: false,
  showPlayMode: false,
  defaultVolume: 50,
  defaultPlayMode: 'shufflePlay',
  defaultPosition: {
    bottom: 20,
    right: 20,
  },
};

function MusicPlayer() {
  return (
    <ReactJkMusicPlayer {...options} />
  );
}

export default MusicPlayer;
