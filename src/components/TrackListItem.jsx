import React from 'react';
import PropTypes from 'prop-types';

const containerStyle = {
  padding: '12px 0',
};

const nameStyle = {
  fontFamily: "'Oswald', sans-serif",
  fontSize: '16px',
  color: '#222'
};

const artistStyle = {
  color: '#777',
};

function TrackListItem({ name, artist }) {
  return (
    <div style={containerStyle}>
      <div style={nameStyle}>{name}</div>
      <div style={artistStyle}>{artist}</div>
    </div>
  );
}

TrackListItem.propTypes = {
  name: PropTypes.string.isRequired,
  artist: PropTypes.string.isRequired,
};

export default TrackListItem;
