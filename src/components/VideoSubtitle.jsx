import React from 'react';
import PropTypes from 'prop-types';
import { Grid } from '@material-ui/core';

const textStyle = {
  fontSize: '20px',
  textAlign: 'center',
  color: 'white',
  padding: '20px 0',
};

function VideoSubtitle({children}) {
  return (
    <Grid container justify="center">
      <Grid item xs={12} md={10} xl={8}>
        <div style={textStyle}>{children}</div>
      </Grid>
    </Grid>
  );
}

VideoSubtitle.propTypes = {
  children: PropTypes.node.isRequired,
}

export default VideoSubtitle;
