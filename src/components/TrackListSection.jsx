import React from 'react';
import { Grid } from '@material-ui/core';

import Photo from './Photo';
import PageSection from './PageSection';

const headerStyle = {
  color: '#292929',
  fontSize: '43px',
  fontWeight: '800',
  fontFamily: "'Playfair Display', sans-serif",
  letterSpacing: '2px',
}

const textStyle = {
  color: '#858585',
  fontSize: '16px',
  fontWeight: '300',
  fontFamily: "'Montserrat', sans-serif",
};

function TrackListSection() {
    return (
      <PageSection color="#f6f6f6">
        <Grid container justify="center">
          <Grid item xs={12} sm={12} md={6} lg={5} xl={4}>
            <div style={{height: '100%', display: 'flex', flexDirection: 'column', alignItems: 'center', justifyContent: 'center'}}>
                <div style={headerStyle}>Track List</div>
                <div style={textStyle}><i>Comming Soon</i></div>
            </div>
          </Grid>
          <Grid item xs={12} sm={12} md={6} lg={5} xl={4}>
            <div style={{textAlign: 'center'}}>
              <Photo url="https://via.placeholder.com/540x640" />
            </div>
          </Grid>
        </Grid>
      </PageSection>
    );
}

export default TrackListSection;
