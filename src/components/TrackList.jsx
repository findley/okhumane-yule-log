import React from 'react';
import PropTypes from 'prop-types';
import { List } from '@material-ui/core';

const containerStyle = {
  padding: '20px',
}

function TrackList({ children }) {
  return (
    <div style={containerStyle}>
      <List>
        {children}
      </List>
    </div>
  );
}

TrackList.propTypes = {
  children: PropTypes.node,
};

export default TrackList;
