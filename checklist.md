- [x] Add new images in new directory
    - new file /public/images/2020/bandcamp.png
    - new file /public/images/2020/chonker.jpg
    - new file /public/images/2020/credits.png
    - new file /public/images/2020/doggo.jpg
    - new file /public/images/2020/kitty.jpg
    - new file /public/images/2020/poster.jpg
    - new file /public/images/2020/pupper.jpg
    - new file /public/images/2020/track-list.png
    - new file /public/images/2020/youtube.png

- [x] Create new MainPage component
    - new file /src/pages/MainPage2020/MainPage2020.jsx
    - [x] Update donation link
    - [x] Update album link
    - [x] Update youtube link
    - [x] Update video player source url
    - [x] Update link to previous yule log

- [x] Upload new video to CDN
    `s3cmd put 2021-yule-log.mp4 s3://okhumane/`

- [x] Update router in index to render new MainPage component
    ```
    diff --git a/src/index.js b/src/index.js
    +      <Route path="/2019">
    +        <MainPage2019 />
    +      </Route>
    +
    +      <Route path="/2020">
    +        <MainPage2020 />
    +      </Route>
    +
           <Route path="/">
    -        <MainPage />
    +        <MainPage2020 />
           </Route>
   ```
