package main

import (
	"encoding/json"
	"net/http"
	"sync"
	"os"
	"log"
)

type requestBody struct {
	Email string `json:"email"`
}

type fileDb struct {
	File string
	sync.Mutex
}

func newFileDb(filePath string) *fileDb {
	return &fileDb{File: filePath}
}

func (db *fileDb) write(val string) error {
	db.Lock()
	defer db.Unlock()
	f, err := os.OpenFile(db.File, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		return err
	}
	defer f.Close()

	_, err = f.WriteString(val + "\n")
	if err != nil {
		return err
	}

	return nil
}

func main() {
	db := newFileDb("./emails.db");

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		decoder := json.NewDecoder(r.Body)
		var data requestBody
		err := decoder.Decode(&data)
		if err != nil {
			http.Error(w, "Failed to decode request body", 400);
			return
		}
		
		err = db.write(data.Email)
		if err != nil {
			http.Error(w, "Server error", 500)
			log.Printf("%#v", err)
		}
	})

	log.Fatal(http.ListenAndServe(":4444", nil))
}
