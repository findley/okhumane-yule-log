# OK Humane Yule Log Website

## Updating for new year
- Create a copy of last year's `src/components/pages/MainPage20XX` component.
- Update routing in index.js to show the new page
- Upload assets to `public/images/20XX` and update the MainPage component as
  necessary.
- Upload the video to digitalocean spaces: `s3cmd put ~/Downloads/2023-yule-log.mp4 s3://okhumane/`
